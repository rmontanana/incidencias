<?php
//
// Esta clase procesar� una p�gina sustituyendo
// las marcas {} por el contenido que proceda.
//
// El constructor recibe el nombre del archivo que
// act�a como plantilla de distribuci�n del contenido
// y una referencia al objeto cuyos m�todos deber�n
// aportar los contenidos.
//
class distribucion {
	// Variable para conservar la plantilla
	private $plantilla;
	// Matriz que contendr� los nombres de elementos
	private $elementos;
	// Referencia al objeto cuyos m�todos ser�n
	// invocados para aportar el contenido
	private $objeto;
	// Constructor de la clase
	public function __construct($archivo, $objeto)
	{
		// Recuperamos el contenido del archivo
		$this->plantilla=file_get_contents($archivo)
			or die('Fallo en la apertura de la plantilla');
		// y guardamos la referencia al objeto
		$this->objeto=$objeto;
		// Extraemos todas las marcas de contenido
		preg_match_all('/\{[A-Za-z]+\}/', $this->plantilla,$el, PREG_PATTERN_ORDER);
		// Nos quedamos con la matriz de resultados
		$this->elementos=$el[0];
	}
	// Este m�todo es el encargado de procesar la plantilla
	public function procesaPlantilla()
	{
		// Tomamos la plantilla en una variable local, para
		// as� no modificar el contenido original
		$pagina=$this->plantilla;
		// Recorremos la matriz de marcas de contenido
		foreach($this->elementos as $el) {
			// Eliminamos los delimitadores { y }
			$el=substr($el,1,strlen($el)-2);
			// invocamos a la funci�n del objeto
			$resultado=$this->objeto->$el();
			// e introducimos su contenido en lugar de la marca
			$pagina=str_replace('{'.$el.'}',$resultado,$pagina);
		}
		// Si es posible comprimir
		/*if(strstr($_SERVER['HTTP_ACCEPT_ENCODING'],'gzip')) {
			// introducimos la cabecera que indica que el contenido est� comprimido
			header('Content-Encoding: gzip');
			// y comprime al m�ximo la informaci�n antes de enviarla
			return gzencode($pagina, 9);
		}*/
		return $pagina; // enviamos sin comprimir
	}
}
?>
