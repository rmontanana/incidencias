<?php
// Incluimos los m�dulos necesarios
function __autoload($class_name) {
   require_once $class_name . '.php';
}
include('inc/configuracion.inc');

$aplicacion=new Incidencias();
if ($aplicacion->estado())
	$aplicacion->Ejecuta();
// Clase del objeto principal de la aplicaci�n
class incidencias {
	// Declaraci�n de miembros
	private $bdd; // Enlace con el SGBD
	private $registrado; // Usuario registrado s/n
	private $usuario; // Nombre del usuario
	private $clave; //contrase�a del usuario
	private $opcActual; // Opci�n elegida por el usuario
	private $estado;
	// Constructor
	public function __construct()
	{
		// Analizamos la cadena de solicitud para saber
		// qu� opci�n es la actual
		$this->opcActual=$_SERVER['QUERY_STRING']=='' ? 'principal' : $_SERVER['QUERY_STRING'];
		// Iniciamos una sesi�n
		session_start();
		//Conexi�n con la base de datos.
		$this->bdd=new mysqli(SERVIDOR,USUARIO,CLAVE,BASEDATOS);
		if (mysqli_connect_errno()) {
			echo '<h1>Fallo al conectar con el servidor MySQL.</h1>';
			$this->estado=false;
			return;
		} else
			$this->estado=true;
		// Comprobamos si el usuario ya est� registrado en esta sesi�n
		$this->registrado=isset($_SESSION['Registrado']);
		if($this->registrado) // si est�...
			// recuperamos el nombre del usuario
			$this->usuario=$_SESSION['Usuario'];
		// en caso contrario comprobamos si tiene la cookie que le identifica como usuario
		elseif(isset($_COOKIE['IncidenciasId']))
			// y usamos el Id para recuperar el nombre de la base de ddtos
			$this->recuperaNombreConId($_COOKIE['IncidenciasId']);
		else // en caso contrario el usuario no est� registrado
			$this->usuario='';
	}
	public function estado()
	{
		return $this->estado;
	}
	public function __destruct()
	{
		//En principio no es necesario, pero conceptualmente es lo correcto
		//ya que no est� documentado el destructor de la clase mysqli
		$this->bdd->close();
	}
	// Esta funci�n pondr� en marcha la aplicaci�n ocup�ndose
	// de las acciones que no generan contenido, esto es
	// iniciar sesi�n, cerrarla, etc.
	public function Ejecuta()
	{
		// Dependiendo de la opci�n a procesar
		switch($this->opcActual) {
			// El usuario quiere iniciar sessi�n
			case 'registrarse':
				// Se identifica como usuario de la aplicaci�n
				// si no se ha identificado antes
				$_SERVER['PHP_AUTH_USER']='demo';
				$_SERVER['PHP_AUTH_PW']='demo';

				if(isset($_SERVER['PHP_AUTH_USER'])) {
					// si lo hizo con anterioridad...
					// Buscamos el usuario en la base de datos y obtenemos
					// su Id de sesi�n
					$resultado=$this->usuarioRegistrado();
					// Si lo obtuvimos es que el usuario est� registrado
					if($resultado) {
						// establecemos las variables de sesi�n
						$_SESSION['Registrado'] = TRUE;
						$_SESSION['Usuario'] = $this->usuario;
						// y enviamos la cookie para reconocerlo la pr�xima vez
						setcookie('InicidenciasId', $resultado, time()+3600*24*365);
						// Lo enviamos a la p�gina de bienvenida
						header('Location: index.php?bienvenido');
						exit;
					}
				}
				// debe hacerlo ahora.
				header('WWW-Authenticate: Basic realm="Incidencias"');
				header('HTTP/1.0 401 Unauthorized');
				echo ' Debes introducir un nombre de usuario y clave v&aacute;lidos.';
				exit;
			// El usuario quiere cerrar la sesi�n actual
			case 'cerrarSesion':
				// Eliminamos los datos de sesi�n
				session_unset();
				session_destroy();
				// y borramos la cookie para no reconocerlo
				// la pr�xima vez
				setcookie('IncidenciasId', '');
				// y le redirigmos a la p�gina inicial
				header('Location: index.php');
				exit;
			default:
				// Creamos un objeto Distribuci�n facilit�ndole el
				// nombre del archivo plantilla y el objeto que aportar�
				// el contenido
				$opc=$_GET['opc'];
				list($opcion,$parametro)=explode("&",$this->opcActual);
				switch ($opc) {
					case 'informe':
						$enlace='xml/'.$opcion.'.xml';
						$informe=new informePDF($this->bdd,$enlace,$this->registrado);
						return;
					default:
						$plant='plant/principal.html';
						$salida=new distribucion($plant,
							new aportaContenido($this->bdd,$this->registrado,$this->usuario,$this->opcActual));
						echo $salida->procesaPlantilla();
						break;
				}
	  }
	}
	// Esta funci�n comprueba si el usuario est� o no registrado,
	// devolviendo su IdSesion en caso afirmativo o FALSE
	// en caso contrario
	private function usuarioRegistrado()
	{
		// Nombre del usuario buscado
		$this->usuario=$_SERVER['PHP_AUTH_USER'];
		// clave que ha facilitado
		$this->clave=$_SERVER['PHP_AUTH_PW'];
		//echo "Llamada a usuarioRegistrado con usuario=$this->usuario y clave=$this->clave ";
		// y ejecutar la consulta para buscar el usuario
		$resultado=$this->bdd->query("SELECT * FROM Usuarios WHERE nombre='$this->usuario'");
		// Si no hemos encontrado el usuario
		if($resultado->num_rows==0)
			return FALSE; // devolvemos FALSE
		// En caso contrario obtenemos en una matriz
		// los datos de las columnas
		$fila=$resultado->fetch_assoc();
		// Para comprobar si la clave coincide
		if($fila['clave']==$this->clave)
			// caso en el que devolveremos un Id de sesi�n
			// �nico para este usuario
			return $fila['idSesion'];
		// Si llegamos aqu� es porque el usuario existe pero
		// la clave no es correcta
		header('WWW-Authenticate: Basic realm="Clave incorrecta"');
		header('HTTP/1.0 401 Unauthorized');
		echo 'Debe introducir un nombre de usuario y clave v&aacute;lidos.'."fila=".$fila['clave']." clave=".$this->clave;
		echo "fila=".$fila['clave']." clave=".$this->clave;
		exit;
	}
	// Esta funci�n intenta recuperar el nombre del usuario
	// a partir del Id de sesi�n almacenado en una cookie,
	// dejando las variables bRegistrado y sUsuario con
	// los valores apropiados
	private function recuperaNombreConId($idSesion)
	{
		// para ejecutar la consulta para buscar el Id de sesi�n
		$resultado=$this->bdd->query("SELECT nombre FROM Usuarios WHERE idSesion='$idSesion'");
		// Si no hemos encontrado el ID
		if($resultado->num_rows==0) {
		// el usuario no est� registrado
			$this->usuario='';
			$this->registrado=FALSE;
		} else {
			// En caso contrario obtenemos en una matriz
			// los datos de las columnas
			$fila=$resultado->fetch_assoc();
			// Asignamos el nombre
			$this->usuario=$fila['nombre'];
			// damos el valor TRUE a bRegistrado
			$this->registrado=TRUE;
			// y establecemos las variables de sessi�n para no tener
			// que efectuar todo este proceso de nuevo con cada
			// solicitud de p�gina
			$_SESSION['Registrado']=TRUE;
			$_SESSION['Usuario']=$this->usuario;
		}
	}
}
?>
