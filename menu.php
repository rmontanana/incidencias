<?php
//
// Esta clase generar� el men� de la aplicaci�n.
class menu {
	private $opciones;
	public function __construct($fichero)
	{
		$contenido=@file_get_contents($fichero) or
			die("<h1>No puedo generar el men�. No puedo acceder al fichero $fichero</h1>");
		// Obtenemos la lista de pares Opci�n|Enlace
		$elementos=explode("\n", $contenido);
		foreach($elementos as $elemento) {
			list($tipo, $opcion, $enlace)=explode('|', $elemento);
			// Los guardamos en la matriz de opciones
			if ($tipo)
				$this->opciones[]=$tipo.",".$opcion.",".$enlace;
		}
	}
	public function insertaMenu()
	{
		$salida="";
		reset($this->opciones);
		foreach($this->opciones as $opcion) {
			list($tipo,$opcion,$enlace)=explode(",",$opcion);
			if ($tipo==2)
				$salida.='<a href="'.$enlace.'">'.$opcion.'</a><br />';
			else
				$salida.='<label class="key">'.$opcion.'</label><br/>';
		}
		return $salida;
	}
}
?>
