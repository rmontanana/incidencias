<?php
//Es una especialización de mantenimiento.
class gestion extends mantenimiento{
	
	public function __construct($baseDatos)
	{
		parent::__construct($baseDatos,"incidencias");
	}
	public function ejecuta()
	{
		$opc=$_GET['opc'];
		$id=$_GET['id'];
		//sig o ant
		$op=$_GET['op'];
		//campo por el que ordenar la consulta
		$orden=isset($_GET['orden'])?$_GET['orden']:'fecha';
		//ascendente o descendente
		$sentido=isset($_GET['sentido'])?$_GET['sentido']:'asc';
		switch ($opc) {
			case 'inicial':
				return $this->consulta($id,$op,$orden,$sentido);
			case 'editar':
				return $this->muestra($id,EDICION);
			case 'eliminar':
				return $this->muestra($id,BORRADO);
			case 'nuevo':
				return $this->muestra(null,ANADIR);
			case 'insertar':
				return $this->insertar();
			case 'modificar':
				return $this->modificar($id);
			case 'borrar':
				return $this->borrar($id);
			case 'informe':
				return $this->informe();
			case 'cerrar':
				return $this->cerrar($id);
			default:
				return 'La clase gestion No entiende lo solicitado.';
		}
	}
	//La consulta es distinta de la general ya que va ordenada por fechas y
	//no mostramos el id, además hay más botones de opciones.
	private function consulta($fec,$op,$orden,$sentido)
	{
		//Esto es necesario ya que no se puede poner un alias en la clausula where
		switch ($orden) {
			case "ubicacion":$campoWhere="U.Descripcion";break;
			case "elemento":$campoWhere="E.Descripcion";break;
			case "proveedor":$campoWhere="P.Descripcion";break;
			case "descripcion":$campoWhere="I.Descripcion";break;
			case "fecha":$campoWhere="fecha";break;
		}
		$opSig=$sentido=='desc'?'<=':'>=';
		$opAnt=$sentido=='desc'?'>=':'<=';
		switch ($op) {
			case "sig":$sufijo="and $campoWhere $opSig\"$fec\"";break;
			case "ant":$ix=$fec;$sufijo="and $campoWhere $opAnt\"$ix\"";break;
			default:$sufijo="";
		}
		//Tratamiento de las claves foráneas
		list($existen,$respuesta)=$this->obtieneClavesForaneas();
		if ($existen) {
			foreach ($respuesta as $linea) {
				list($campo,$tabla,$atributo)=explode(",",$linea);
				$foraneas[$campo]=$tabla;
			}
		}
		//Consulta paginada de las incidencias abiertas.
		//$comando="select id,fecha,id_elemento,id_ubicacion,descripcion,id_proveedor ".
		//			"from Incidencias where isnull(fechaResolucion) $sufijo order by fecha desc limit ".NUMFILAS;
		$comando="select I.id,fecha,E.Descripcion as elemento,U.Descripcion as ubicacion,I.descripcion,P.Descripcion as proveedor ".
					"from Incidencias I inner join Elementos E on id_elemento=E.id inner join Ubicaciones U on id_ubicacion=U.id ".
					"inner join Proveedores P on id_proveedor=P.id where (isnull(fechaResolucion) or fechaResolucion='00-00-000') $sufijo order by ".$orden." ".$sentido." limit ".NUMFILAS;
		$resultado=$this->bdd->query($comando);
		if (!$resultado)
			return $this->errorBD("","No se pudo ejecutar la consulta $comando en la base de datos");
		if ($resultado->num_rows==0) {
			//Evita un bucle infinito
			if ($fec!="" && $op!="")
				return $this->consulta("","",$orden,$sentido);
			//else
			//	return "<h1>No se pudo ejecutar la consulta $comando.</h1>";
		}
		//Prepara la salida de datos en una tabla.
		//En la cabecera los nombres de los campos
		$salida='<p align="center"><table border=1 class="tablaDatos"><tbody>';
		$i=0;
		$primero=true;
		while ($campo=$resultado->fetch_field()) {
			if($primero) {
				//Saltamos el id que no lo queremos en pantalla
				$primero=false;
				continue;
			}
			//Si es una clave foránea pondrá el nombre de la tabla
			if ($foraneas[$campo->name]) {
				$listaClaves[$i]=$foraneas[$campo->name];
				$dato=$foraneas[$campo->name];
			} else
				$dato=ucfirst($campo->name);
			$i+=1;
			$salida.="<th><b><a title=\"Establece orden por $dato\" href=\"$this->url&orden=".strtolower($dato)."\"> $dato </a></b></th>\n";
		}
		$salida.="<th><b> Acci&oacute;n </b></th>";
		//En el cuerpo los datos
		$primero=true;
		while($fila=$resultado->fetch_assoc()) {
			$salida.="<tr>";
			$resultado->field_seek(0);
			$idSig=$fila[$orden];
			$id=$fila['id'];
			//Se queda con la fecha mayor
			if ($primero) {
				$primero=false;
				$idAnt=$idSig;
			}
			$i=0;
			$primer=true;
			while($campo=$resultado->fetch_field()) {
				if ($primer) {
					//Se debe saltar el primer campo que es el id y no lo queremos en pantalla.
					$primer=false;
					continue;
				}
				if ($listaClaves[$i])
					$dato=$this->obtenerDescripcion($listaClaves[$i],$fila[$campo->name]);
				else
					$dato=$fila[$campo->name];	
				$salida.="<td>".$dato."</td>";
				$i+=1;
			}
			//Icono de editar
			$iconoEditar='<a href="index.php?incidencias&opc=editar&id='.$id.
				'"><img title="Editar" src="img/editar.png" alt="editar"></a>';
			//Icono de eliminar
			$iconoEliminar='<a href="index.php?incidencias&opc=eliminar&id='.$id.
				'"><img title="Eliminar" src="img/eliminar.png" alt="eliminar"></a>';
			//Icono de avisos
			$iconoAvisos='<a href="index.php?avisos&opc=inicial&idx='.$id.
				'"><img title="Avisos [n&uacute;mero]" src="img/avisos.png" alt="eliminar"></a>['.$this->numAvisos($id).']';
			$iconoCerrar='<a href="index.php?incidencias&opc=cerrar&id='.$id.
				'"><img title="Cerrar" src="img/cerrar.png" alt="cerrar"></a>';
			$salida.="<td>$iconoAvisos&nbsp;$iconoEditar&nbsp;&nbsp;$iconoCerrar&nbsp;&nbsp;&nbsp;$iconoEliminar</td></tr>\n";
		}
		$salida.="</tbody></table></p>";
		//Añade botones de comandos
		$enlace='<a href="'.$this->url.'&orden='.$orden.'&sentido='.$sentido.'&id=';
		$anterior=$enlace.$idAnt."&op=ant\"><img title=\"Pag. Anterior\" alt=\"anterior\" src=\"img/anterior.png\"></a>\n";
		$siguiente=$enlace.$idSig."&op=sig\"><img title=\"Pag. Siguiente\" alt=\"siguiente\" src=\"img/siguiente.png\"></a>\n";
		$anadir='<a href="index.php?incidencias&opc=nuevo">'.
				'<img title="A&ntilde;adir registro" alt="nuevo" src="img/nuevo.png"></a>';		
		$az='<a href="'.$this->url.'&orden='.$orden.'&sentido=asc"><img alt="asc" title="Orden ascendente" src="img/ascendente.png"></a>';
		$za='<a href="'.$this->url.'&orden='.$orden.'&sentido=desc"><img alt="desc" title="Orden descendente" src="img/descendente.png"></a>';
		$informe='<a href="index.php?'.$this->tabla.'&opc=informe" target="_blank"><img src="img/informe.png" alt="informe" title="Informe pdf"></a>';
		$salida.='<p align="center">'.
				"$anterior&nbsp&nbsp$az&nbsp&nbsp$anadir&nbsp&nbsp$informe&nbsp&nbsp$za&nbsp&nbsp$siguiente</p>";
		$resultado->close();
		return $salida;
	}
	private function numAvisos($id)
	{
		//Devuelve el número de avisos asociados a una incidencia
		$comando="select count(*) from Avisos where id_incidencia='$id';";
		$resultado=$this->bdd->query($comando);
		if (!$resultado)
			return $this->errorBD($comando);
		$col=$resultado->fetch_row();
		$resultado->close();
		return $col[0];
	}
	private function cerrar($id)
	{
		//Cierra la incidencia
		
		$fecha=strftime("%Y-%m-%d");
		$comando="update Incidencias set fechaResolucion='$fecha' where id='$id';";
		$resultado=$this->bdd->query($comando);
		if (!$resultado)
			return $this->errorBDD($comando);
		header('location: '.$this->url);
	}
	protected function informe()
	{
		//return parent::informe();
		$pdf=new fpdf();
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(40,10,'¡Hola, Mundo!');
		return $pdf->Output('patata.pdf','D');
	}
}
