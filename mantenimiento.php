<?php
//Clase que se encargará de manejar los elementos del modelo de datos.
define (EDICION,'Edici&oacute;n');
define (BORRADO,'<i>Borrado</i>');
define (ANADIR,'Inserci&oacute;n');
class mantenimiento {
	private $id;
	private $descripcion;
	protected $bdd;
	protected $url;
	protected $cabecera;
	protected $tabla;
	
	public function __construct($baseDatos,$nombre)
	{
		$this->bdd=$baseDatos;
		$this->url="index.php?$nombre&opc=inicial";
		$this->cabecera='Location: '.$this->url;
		$this->tabla=$nombre;
	}
	public function ejecuta()
	{
		$opc=$_GET['opc'];
		$id=$_GET['id'];
		$op=$_GET['op'];
		switch ($opc) {
			case 'inicial':
			return $this->consulta($id,$op);
			case 'editar':
				return $this->muestra($id,EDICION);
			case 'eliminar':
				return $this->muestra($id,BORRADO);
			case 'nuevo':
				return $this->muestra(null,ANADIR);
			case 'insertar':
				return $this->insertar(); 
			case 'modificar':
				return $this->modificar($id); 
			case 'borrar':
				return $this->borrar($id); 
			case 'informe':
				return $this->informe();
			default:
				return 'La clase mantenimiento No entiende lo solicitado.';
		}
	}
	protected function informe()
	{
		return 'informe';
	}
	protected function obtieneClavesForaneas()
	{
		$comando="show create table ".ucfirst($this->tabla);
		$resultado=$this->bdd->query($comando);
		$datos=$resultado->fetch_row();
		$linea=$datos[1];
		$lista=explode("FOREIGN KEY",$linea);
		$primero=true;
		$salida="";
		$existen=false;
		foreach($lista as $clave)
			if ($primero)
				//ignora el primero
				$primero=false;
			else {
				$existen=true;
				preg_match_all("/`[A-Za-z_]+`/",$clave,$campos);
				$campos=$campos[0];
				//Se queda con los datos relevantes sin las `
				$campo=substr($campos[0],1,strlen($campos[0])-2);
				$tabla=substr($campos[1],1,strlen($campos[1])-2);
				$atributo=substr($campos[2],1,strlen($campos[2])-2);
				$salida[]=$campo.",".$tabla.",".$atributo;
		}
		$resultado->close();	
		return array($existen,$salida);
	}
	private function consulta($id,$op)
	{
		switch ($op) {
			case "sig":$sufijo="where id>\"$id\"";break;
			case "ant":$ix=$id-NUMFILAS-1;$sufijo="where id>\"$ix\"";break;
			default:$sufijo="";
		}
		//Tratamiento de las claves foráneas
		list($existen,$respuesta)=$this->obtieneClavesForaneas();
		if ($existen) {
			foreach ($respuesta as $linea) {
				list($campo,$tabla,$atributo)=explode(",",$linea);
				$foraneas[$campo]=$tabla;
			}
		}
		//Consulta paginada de todas las tuplas
		$comando="select * from ".ucfirst($this->tabla)." $sufijo limit ".NUMFILAS;
		$resultado=$this->bdd->query($comando);
		if (!$resultado)
			return $this->errorBD("","No se pudo ejecutar la consulta $comando en la base de datos");
		if ($resultado->num_rows==0) {
			//Evita un bucle infinito
			if ($id!="0" && $op!="")
				return $this->consulta("0","");
			else
				return "<h1>No se pudo ejecutar la consulta $comando.</h1>";
		}
		//Prepara la salida de datos en una tabla.
		//En la cabecera los nombres de los campos
		$salida='<p align="center"><table border=1 class="tablaDatos"><tbody>';
		$i=0;
		while ($campo=$resultado->fetch_field()) {
			//Si es una clave foránea pondrá el nombre de la tabla
			if ($foraneas[$campo->name]) {
				$listaClaves[$i]=$foraneas[$campo->name];
				$dato=$foraneas[$campo->name];
			} else
				$dato=ucfirst($campo->name);
			$i+=1;
			$salida.="<th><b> $dato </b></th>\n";
		}
		$salida.="<th><b> Acci&oacute;n </b></th>";
		//En el cuerpo los datos
		$primero=true;
		while($fila=$resultado->fetch_assoc()) {
			$salida.="<tr>";
			$resultado->field_seek(0);
			$id=$fila['id'];
			//Se queda con el id más pequeño
			if ($primero) {
				$primero=false;
				$menorId=$id;
			}
			$i=0;
			while($campo=$resultado->fetch_field()) {
				if ($listaClaves[$i])
					$dato=$this->obtenerDescripcion($listaClaves[$i],$fila[$campo->name]);
				else
					$dato=$fila[$campo->name];	
				$salida.="<td>".$dato."</td>";
				$i+=1;
			}
			//Añade el icono de editar
			$salida.='<td><a href="index.php?'.$this->tabla.'&opc=editar&id='.$id.
				'"><img title="Editar" src="img/editar.png" alt="editar"></a>';
			//Añade el icono de eliminar
			$salida.='&nbsp;&nbsp;<a href="index.php?'.$this->tabla.'&opc=eliminar&id='.$id.
				'"><img title="Eliminar" src="img/eliminar.png" alt="eliminar"></a></td></tr>'."\n";
		}
		$salida.="</tbody></table></p>";
		//Añade botones de comandos
		$enlace='<a href="'.$this->url.'&id=';
		$anterior=$enlace.$menorId."&op=ant\"><img title=\"Pag. Anterior\" alt=\"anterior\" src=\"img/anterior.png\"></a>\n";
		$siguiente=$enlace.$id."&op=sig\"><img title=\"Pag. Siguiente\" alt=\"siguiente\" src=\"img/siguiente.png\"></a>\n";
		$anadir='<a href="index.php?'.$this->tabla.'&opc=nuevo">'.
				'<img title="A&ntilde;adir registro" alt="nuevo" src="img/nuevo.png"></a>';
		$informe='<a href="index.php?'.$this->tabla.'&opc=informe" target="_blank"><img src="img/informe.png" alt="informe" title="Informe pdf"></a>';
		$salida.='<p align="center">'.
				"$anterior&nbsp&nbsp$anadir&nbsp&nbsp$informe&nbsp&nbsp$siguiente</p>";
		$resultado->close();
		return $salida;
	}
	protected function borrar($id,$enlaceExterior)
	{
		$comando="delete from ".ucfirst($this->tabla)." where id=\"$id\"";
		if (!$this->bdd->query($comando))
			return $this->errorBD($comando);
		$enlace=isset($enlaceExterior)?$enlaceExterior:$this->url;
		header('Location: '.$enlace);
		return;
	}
	protected function insertar($enlaceExterior)
	{
		$comando="insert into ".ucfirst($this->tabla)." (";
		$lista=explode("&",$_POST['listacampos']);
		$primero=true;
		//Añade la lista de campos
		foreach ($lista as $campo) {
			if ($campo=="")
				continue;
			if ($primero) {
				$primero=false;
				$coma=" ";
			} else
				$coma=",";
			$comando.="$coma $campo";
		}
		$comando.=") values (";
		//Añade la lista de valores
		$primero=true;
		foreach ($lista as $campo) {
			if ($campo=="")
				continue;
			if ($primero) {
				$primero=false;
				$coma=" ";
			} else
				$coma=",";
			$comando.="$coma \"$_POST[$campo]\"";
		}
		$comando.=")";
		if (!$this->bdd->query($comando))
			return $this->errorBD($comando);
		//header('<meta http-equiv="Refresh" content="'.PAUSA.'; URL='.$this->url.'">');
		$ident=$this->bdd->insert_id - 1;
		$enlace=isset($enlaceExterior)?$enlaceExterior:$this->url."&id=$ident&op=sig";
		return "<h1><a href=\"$enlace\">Se ha insertado el registro con la clave ".$this->bdd->insert_id."</a></h1>";
	}
	protected function modificar($id,$enlaceExterior)
	{
		//Los datos a utilizar para actualizar la tupla vienen en $_POST.
		//La lista de atributos de la tupla viene en el campo oculto listacampos
		$comando="update ".ucfirst($this->tabla)." set ";
		$lista=explode("&",$_POST['listacampos']);
		$primero=true;
		foreach ($lista as $campo) {
			if ($campo=="id" || $campo=="")
				continue;
			if ($primero) {
				$primero=false;
				$coma=" ";
			} else
				$coma=",";
			if (strlen(trim($_POST[$campo]))==0)
				$comando.="$coma $campo=null";
			else
				$comando.=$coma.' '.$campo.'="'.$_POST[$campo].'"';
		}
		$comando.=" where id=\"$id\"";
		if (!$this->bdd->query($comando))
			return $this->errorBD($comando);
		$enlace=isset($enlaceExterior)?$enlaceExterior:$this->url;
		header('Location: '.$enlace);
		return;
	}
	protected function muestra($id,$tipo,$com,$idx)
	{
		$comando=isset($com)?$com:"select * from ".ucfirst($this->tabla)." where id='$id'";
		$resultado=$this->bdd->query($comando);
		if (!$resultado)
			return $this->errorBD("","No se han podido encontrar datos del identificador $id");
		$fila=$resultado->fetch_assoc();
		$idxx=isset($idx)?"idx=$idx&":"";
		$accion="index.php?$this->tabla&".$idxx."id=$id&opc=";
		switch ($tipo) {
			case EDICION:
				$accion.="modificar";
				$modo="";
				break;
			case BORRADO:
				$accion.="borrar";
				$modo="readonly";
				break;
			case ANADIR:
				$accion.="insertar";
				$modo="";
				break;
		}
		$salida=""; //Inicializa la variable a devolver.
		//Tratamiento de las claves foráneas
		list($existen,$respuesta)=$this->obtieneClavesForaneas();
		if ($existen) {
			foreach ($respuesta as $linea) {
				list($campo,$tabla,$atributo)=explode(",",$linea);
				$foraneas[$campo]=$tabla;
			}
		}
		//Genera un formulario con los datos de la tupla seleccionada.
		$salida.='<form name="mantenimiento.form" method="post" action="'.$accion.'">'."\n";
		$salida.="<fieldset style=\"width: 96%;\"><p><legend style=\"color: red;\"><b>$tipo</b></legend>\n";
		while($campo=$resultado->fetch_field()) {
			$foranea=$foraneas[$campo->name] ? true : false;
			$salida.="<label>".ucfirst($campo->name)."</label> ";
			//Se asegura que el id no se pueda modificar.
			$modoEfectivo=$campo->name=='id' ? "readonly" : $modo;
			if (!$foranea) {
				//Si es un campo fecha u hora y está insertando pone la fecha actual o la hora actual
				if ($tipo==ANADIR) {
					if ($campo->name=="fecha")
						$fila[$campo->name]=strftime("%Y-%m-%d");
					elseif ($campo->name=="hora")
							$fila[$campo->name]=strftime("%H:%M:%S");
				}
				//Si no es una clave foránea añade un campo de texto normal
				$salida.='<input type="text" name="'.$campo->name.'" value="'.$fila[$campo->name].
							'" maxlength="'.$campo->length.'" '.$modoEfectivo." ><br><br>\n";
			} else
				$salida.=$this->generaLista($foraneas[$campo->name],$campo->name,$fila[$campo->name],$modoEfectivo);
			//Genera una lista con los campos a modificar.
			$campos.="$campo->name&";
		}
		//genera un campo oculto con la lista de campos a modificar.
		$salida.='<input name="listacampos" type="hidden" value="'.$campos."\">\n";
		$salida.="</fieldset><p>";
		$salida.="<button type=reset>Cancelar</button>&nbsp;&nbsp;<button type=submit>Aceptar</button><br>\n";
		$resultado->close();
		return $salida;
	}
	//Función que genera un campo de lista con los valores de descripción de la
	//tabla a la cual pertenece la clave foránea.
	protected function generaLista($tabla,$campo,$valor,$modo)
	{
		//La tabla debe tener un campo Descripción
		$comando="select id,Descripcion from $tabla order by Descripcion";
		$resultado=$this->bdd->query($comando);
		if (!$resultado)
			return $this->errorBD($comando);
		$modoEfectivo=$modo=="readonly" ? "disabled" : "";
		$salida="<select name=\"$campo\">\n";
		while($fila=$resultado->fetch_assoc()) {
			$dato=$fila['id'];
			$seleccionado=$dato==$valor ? " selected " : "";
			$salida.='<option value="'.$dato.'" '.$seleccionado.$modoEfectivo.' >'.
				$fila['Descripcion']."</option>\n";
		}
		$salida.="</select>\n<br><br>";
		$resultado->close();
		return $salida;
	}
	protected function obtenerDescripcion($tabla,$clave)
	{
		$comando="select Descripcion from $tabla where id=\"$clave\"";
		$resultado=$this->bdd->query($comando);
		if (!$resultado)
			return $this->errorBD($comando);
		$fila=$resultado->fetch_row();
		$salida=$fila[0];
		$resultado->close();
		return $salida;
	}
	protected function errorBD($comando,$mensaje="")
	{
		if (!$mensaje)
			return "<h1>No pudo ejecutar correctamente el comando $comando error=".$this->bdd->error." </h1>";
		else
			return "<h1>$mensaje error=".$this->bdd->error."</h1>";
	}
}
?>