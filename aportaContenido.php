<?php
define (PIE,'<center><a target="_blank" href="http://www.apache.org"><img src="img/apache.gif" alt="Sitio web creado con Apache" /></a>'.
						'<a target="_blank" href="http://www.mysql.org"><img src="img/mysql.png" width=125 height=47 alt="Gestor de bases de datos mySQL" /></a>'.
						'<a target="_blank" href="http://www.php.net"><img src="img/php.gif" alt="PHP Language" /></a> </center>');
// Esta clase aportará el contenido a la plantilla
class aportaContenido {
	private $registrado;
	private $usuario;
	private $miMenu;
	private $bdd;
	private $opcionActual;
	// El constructor necesita saber cuál es la opción actual
	public function __construct($baseDatos,$registrado,$usuario,$opcion)
	{
		$this->bdd=$baseDatos;
		$this->miMenu=new menu('inc/incidencias.menu');
		$this->registrado=$registrado;
		$this->usuario=$usuario;
		$this->opcionActual=$opcion;
  }
	//Funcion que devuelve la fecha actual
	public function fechaActual($formato='',$idioma='es_ES')
	{
		if ($formato=='')
			$formato="%d-%b-%Y %H:%M";
		setlocale(LC_TIME,$idioma);
		return strftime($formato);
	}
	private function mensajeRegistro()
	{
		return 'Debe registrarse para acceder a este apartado';
	}
	// Procesaremos todas las invocaciones a métodos en
	// la función __call()
	public function __call($metodo,$parametros)
	{
		switch($metodo) { // Dependiendo del método invocado
			case 'titulo': // devolvemos el título
				return APLICACION;
			case 'usuario':
				if ($this->registrado)
					return "Usuario=$this->usuario";
				else
					return '';
			case 'fecha': return $this->fechaActual();
			case 'aplicacion': return APLICACION;
			case 'menu': // el menú
				return $this->registrado ? $this->miMenu->insertaMenu() :
					'Puede Iniciar sesi&oacute;n con<br>usuario <i><b>demo</b></i><br>contrase&ntilde;a <i>demo</i><br>';
			case 'opcion':
				list($opcion,$parametro)=explode("&",$this->opcionActual);
				switch($opcion) {
						case 'bienvenido':
							return "Men&uacute; Principal";
						case 'principal':
							return "Pantalla Inicial";
						case 'elementos':
						case 'proveedores':
						case 'ubicaciones':
						case 'personal':
						case 'profesiones':
						case 'incidencias':
						case 'avisos':
							return "Mantenimiento de ".ucfirst($opcion).".";
				}
				return '';
			case 'control':
				if ($this->registrado)
					return '<a href="index.php?cerrarSesion">Cerrar Sesi&oacute;n</a>';
				else
					return '<a href="index.php?registrarse">Iniciar Sesi&oacute;n</a>';
			// Para incluir el contenido central de la página
			case 'contenido':
				// tendremos en cuenta cuál es la opción actual
				/*echo "opcActual=$this->opcActual<br>";
				echo "Metodo=$Metodo<br>";
				print_r($Parametros);*/
				list($opcion,$parametro)=explode("&",$this->opcionActual);
				switch($opcion) {
					case 'principal': // contenido inicial
						return '<br><br><center><img src="img/logo.png" alt="Gesti&oacute;n de Incidencias">'.
							'<br><label>Gesti&oacute;n de Incidencias</label></center><br><br>'.PIE;
					case 'elementos': 
					case 'proveedores':
					case 'ubicaciones':
					case 'profesiones':
					case 'personal':
						if ($this->registrado) {
							$ele=new mantenimiento($this->bdd,$opcion);
							return $ele->ejecuta();
						} else
							return $this->mensajeRegistro();
					case 'incidencias':
						if ($this->registrado) {
							$ele=new gestion($this->bdd);
							return $ele->ejecuta();
						} else
							return $this->mensajeRegistro();
					case 'avisos':
						if ($this->registrado) {
							$id=$_GET['idx'];
							$ele=new avisos($this->bdd,$id);
							return $ele->ejecuta();
						} else
							return $this->mensajeRegistro();
					case 'bienvenido': // El usuario quiere iniciar sesión
						return 'Bienvenido '.$this->usuario.'<br><br><center><img src="img/corteagua.png" alt="Gesti&oacute;n de Incidencias">'.
							'<br><label>Gesti&oacute;n de Incidencias</label></center><br><br>'.PIE;
		    } // Fin del contenido
			case 'registro': // Si está registrado mostrar bienvenida
				// si no, un enlace
				if($this->bEstaRegistrado)
					return "Bienvenido <b>$this->sUsuario</b><hr />".
						'<a href="index.php?cerrarSesion">Cerrar sesi&oacute;n</a>';
					else
						return '';
			default: // Si es cualquier otra marca
				return "Marca {$metodo} queda sin procesar";
		}
	}
}
?>