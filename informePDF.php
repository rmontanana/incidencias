<?php
 //
  // Clase informePDF
  //
  // Esta clase genera un documento PDF a partir
  // de una descripción dada en una archivo XML
  //
  class informePDF
  {
    private $bdd;
    // El constructor recibe como argumento el nombre
    // del archivo XML con la definición, encargándose
    // de recuperarla y guardar toda la información localmente
    public function __construct($bdd,$definicion,$registrado)
    {
      if (!$registrado)
        return 'Debe registrarse para acceder a este apartado';
      $this->bdd=$bdd;
      // Recuperamos la definición del informe
      $def=simplexml_load_file($definicion);
      //print_r($def);
      // Iniciamos la creación del documento
      $pdf=new pdf_mysql_table($this->bdd,(string)$def->Pagina['Orientacion'],
              (string)$def->Pagina['Formato'],
              (string)$def->Titulo,(string)$def->Pagina->Cabecera);
      //echo $def->Titulo.$def->Cabecera;
      $pdf->Open();
      $pdf->setAuthor(utf8_decode('Ricardo Montañana Gómez'));
      $pdf->setCreator(html_entity_decode(APLICACION));
      $pdf->setSubject(utf8_decode($def->Titulo));
      $pdf->setAutoPageBreak(false);
      $filas=$this->bdd->query(trim($def->Datos->Consulta));
      $pdf->AddPage();
      // Recuperamos los datos del cuerpo
      foreach($def->Pagina->Cuerpo->Col as $columna)
        $pdf->AddCol((string)$columna['Nombre'],(string)$columna['Ancho'],
                      (string)$columna['Titulo'],(string)$columna['Ajuste']);
      $prop=array('HeaderColor'=>array(255,150,100),
            'color1'=>array(210,245,255),
            'color2'=>array(255,255,210),
            'padding'=>2);
      $pdf->Table($def->Datos->Consulta,$prop);
      $pdf->Close();
      // Obtenemos el documento y su longitud
      $documento=$pdf->Output('','S');
      $longitud=strlen($documento);
      // y lo enviamos como resultado
      header("Content-type: application/pdf");
      header("Content-length: $longitud");
      header("Content-Disposition: inline; filename=Informe.pdf");
      echo $documento;
    }
  }
?>
