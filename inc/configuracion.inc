<?php
/* Fichero de configuración donde se introducirán los valores para
   la conexión con el servidor MySQL.
   Ver. 0.1
   Copyright (C) 2005 Ricardo Montañana Gómez
   Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la Free Software Foundation, bien de la versión 2 de dicha Licencia o bien (según su elección) de cualquier versión posterior.

Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.

Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, escriba a la Free Software Foundation, Inc., en 675 Mass Ave, Cambridge, MA 02139, EEUU.
*/
define(SERVIDOR,'localhost'); //Ubicación del servidor MySQL
define(BASEDATOS,'Incidencias'); //Nombre de la base de datos.
define(USUARIO,'incidencias'); //Usuario con permisos de lectura/escritura en la base de datos
define(CLAVE,'incidecnias'); //contraseña del usuario.
define(VERSION,'0.1prealfa');
define(APLICACION,'I.E.S.O. Pascual Serrano. Gesti&oacute;n de Incidencias '.VERSION);
define(NUMFILAS,10); // Número de registros a mostrar en las pantallas de consulta iniciales
define(PAUSA,2);//Nº segundos de pausa para mostrar mensaje id insertado
error_reporting(0); //desactiva los warnings y errores del php
?>
