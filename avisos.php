<?php
//Es una especialización de mantenimiento.
class avisos extends mantenimiento {
	private $id_incidencia;
	
	public function __construct($baseDatos,$id)
	{
		parent::__construct($baseDatos,"avisos");
		$this->id_incidencia=$id;
	}
	public function ejecuta()
	{
		$opc=$_GET['opc'];
		$id=$_GET['id'];
		$op=$_GET['op'];
		$enlace=$this->url."&idx=".$this->id_incidencia;
		$comando="select fecha,hora,comentario from Avisos where id='$id';";
		switch ($opc) {
			case 'avisos':
			case 'inicial':
				return $this->consulta();
			case 'editar':
				return $this->muestra($id,EDICION,$comando,$this->id_incidencia);
			case 'eliminar':
				return $this->muestra($id,BORRADO,$comando,$this->id_incidencia);
			case 'nuevo':
				return $this->muestra(null,ANADIR,$comando,$this->id_incidencia);
			case 'insertar':
				return $this->insertar($enlace); 
			case 'modificar':
				return $this->modificar($id,$enlace); 
			case 'borrar':
				return $this->borrar($id,$enlace); 
			default:
				return 'La clase avisos No entiende lo solicitado.';
		}
	}
	private function cabeceraTabla($titulo,$resultado,$anadido)
	{
		//Obtiene los nombres de los campos del resultado.
		$resultado->field_seek(0);
		$salida='<p align="center"><table border=1 class="tablaDatos"><tbody>';
		$i=0;
		$salida.="<th colspan=15><b>$titulo</b></th>\n<tr>";
		while ($campo=$resultado->fetch_field()) {
			$dato=ucfirst($campo->name);
			$i+=1;
			$salida.="<td><b> $dato </b></td>";
		}
		if (isset($anadido))
			$salida.="<td><b> $anadido </b></td>";
		$salida.="</tr>\n";
		return $salida;
	}
	private function datosTabla($resultado,$fila)
	{
		//Obtiene los datos de un registro en forma de línea de tabla
		$salida.="<tr>";
		$resultado->field_seek(0);
		$i=0;
		while($campo=$resultado->fetch_field()) {
			$dato=$fila[$campo->name];	
			$salida.="<td>".$dato."</td>";
			$i+=1;
		}
		return $salida;
	}
	public function consulta()
	{
				//Saca los datos de la Incidencia
		$comando="select I.id,fecha,E.Descripcion as elemento,U.Descripcion as ubicacion,I.descripcion,P.Descripcion as proveedor ".
					"from Incidencias I inner join Elementos E on id_elemento=E.id inner join Ubicaciones U on id_ubicacion=U.id ".
					"inner join Proveedores P on id_proveedor=P.id where I.id='$this->id_incidencia'";
		$resultado=$this->bdd->query($comando);
		if (!$resultado)
			return $this->errorBD("","No se pudo ejecutar la consulta $comando en la base de datos");
		$salida=$this->cabeceraTabla("Incidencia",$resultado);
		$fila=$resultado->fetch_assoc();
		$salida.=$this->datosTabla($resultado,$fila);
		$salida.="</tr>\n";
		$salida.="</tbody></table></p>\n";
		//Ahora debe sacar los datos de los avisos.
		$comando="select id,fecha,hora,comentario from Avisos where id_incidencia='$this->id_incidencia' order by fecha desc,hora desc";
		$resultado=$this->bdd->query($comando);
		if (!$resultado)
			return $this->errorBD("","No se pudo ejecutar la consulta $comando en la base de datos");
		$salida.=$this->cabeceraTabla("Avisos",$resultado,"Acci&oacute;n");
		while ($fila=$resultado->fetch_assoc()) {
			$salida.=$this->datosTabla($resultado,$fila);
			$id=$fila['id'];
			//Icono de editar
			$iconoEditar='<a href="index.php?avisos&idx='.$this->id_incidencia.'&opc=editar&id='.$id.
				'"><img title="Editar" src="img/editar.png" alt="editar"></a>';
			//Icono de eliminar
			$iconoEliminar='<a href="index.php?avisos&opc=eliminar&idx='.$this->id_incidencia.'&id='.$id.
				'"><img title="Eliminar" src="img/eliminar.png" alt="eliminar"></a>';
			$salida.="<td>$iconoEditar&nbsp;&nbsp;$iconoEliminar</td></tr>\n";
		}
		$salida.="</tbody></table></p>\n";
		//Añade botones de comandos
		$enlace='<a href="'.$this->url.'&id=';
		$anadir='<a href="index.php?'.$this->tabla.'&opc=nuevo&idx='.$this->id_incidencia.'&id='.$this->id_incidencia.'">'.
				'<img title="A&ntilde;adir registro" alt="nuevo" src="img/nuevo.png"></a>';
		$informe='<a href="index.php?'.$this->tabla.'&opc=informe" target="_blank"><img src="img/informe.png" alt="informe" title="Informe pdf"></a>';
		$salida.='<p align="center">'.
				"$anterior&nbsp&nbsp$anadir&nbsp&nbsp$informe&nbsp&nbsp$siguiente</p>";
		$resultado->close();
		return $salida;
	}
	protected function insertar($enlace)
	{
		//Añade los datos de la clave foránea
		$_POST['listacampos'].="id_incidencia&";
		$_POST['id_incidencia']=$this->id_incidencia;
		return parent::insertar($enlace);
	}
}
?>